/*
    Main.java

    24.01.2019

    version 1.0
 */
package com.igor;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Class for description filter
 *
 * @author igorsas
 * @version 1.0
 * @since 24.01.2019
 */
public class Filter {
    /**
     * variable for category
     */
    private Category category;
    /**
     * variable for minimal price
     */
    private double priceMinimum;
    /**
     * variable for maximum price
     */
    private double priceMaximum;
    /**
     * list of products, which satisfy condition on the filter
     */
    private ArrayList<Product> products;

    /**
     * Constructor with parameters.
     * For creating object of class Filter
     * @param category selected category
     * @param priceMinimum selected minimal price
     * @param priceMaximum selected maximal price
     */
    public Filter(Category category, double priceMinimum, double priceMaximum) {
        this.category = category;
        this.priceMinimum = priceMinimum;
        this.priceMaximum = priceMaximum;
        this.products = new ArrayList<>();
        for(Product product : category.getProducts()){
            if(product.getPrice() >= priceMinimum &&
                    product.getPrice() <= priceMaximum){
                this.products.add(product);
            }
        }
    }

    /**
     * Overrided method for getting info about object in String
     * @return information about object in String
     */
    @Override
    public String toString(){
        StringBuilder string = new StringBuilder("You chose filter: \nCategory: " +
                category.getName() + "\nMinimum price: " +
                priceMinimum + "\nMaximum price: " + priceMaximum + "\n\nProducts:");
        for(Product product : products){
            string.append(product);
        }
        return string.toString();
    }

    /**
     * Overrided method for equals two objects class Filter
     * @param o object for equals with object which called this method
     * @return true if objects is equals and false if not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Filter)) return false;
        Filter filter = (Filter) o;
        return Double.compare(filter.getPriceMinimum(), getPriceMinimum()) == 0 &&
                Double.compare(filter.getPriceMaximum(), getPriceMaximum()) == 0 &&
                Objects.equals(getCategory(), filter.getCategory()) &&
                Objects.equals(getProducts(), filter.getProducts());
    }

    /**
     * Overrided method for identification uniq object
     * Uses where we sorted objects in Collection
     * @return uniq hash code for object
     */
    @Override
    public int hashCode() {
        return Objects.hash(getCategory(), getPriceMinimum(), getPriceMaximum(), getProducts());
    }

    /**
     * method for getting category from current filter
     * @return category from current filter
     */
    public Category getCategory() {
        return category;
    }

    /**
     * method for getting minimum price in the filter
     * @return minimum price in the filter
     */
    public double getPriceMinimum() {
        return priceMinimum;
    }

    /**
     * method for setting new minimum price of the product
     * @param priceMinimum new minimum price of the product
     */
    public void setPriceMinimum(double priceMinimum) {
        this.priceMinimum = priceMinimum;
    }

    /**
     * method for getting maximum price in the filter
     * @return maximum price in the filter
     */
    public double getPriceMaximum() {
        return priceMaximum;
    }

    /**
     * method for setting new maximum price of the product
     * @param priceMaximum new maximum price of the product
     */
    public void setPriceMaximum(double priceMaximum) {
        this.priceMaximum = priceMaximum;
    }

    /**
     * method for getting list of products, which satisfy condition on the filter
     * @return list of products, which satisfy condition on the filter
     */
    public ArrayList<Product> getProducts() {
        return products;
    }

    /**
     * method for adding new product, which satisfies condition on the filter
     * @param product new product, which satisfies condition on the filter
     */
    public void addProduct(Product product){
        this.products.add(product);
    }

}
