/*
    Main.java

    24.01.2019

    version 1.0
 */
package com.igor;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Class for general logic hypermarket
 *
 * @author igorsas
 * @version 1.0
 * @since 24.01.2019
 */
public class Main {
    /**
     * This method creates customer object, that makes an order.
     * Reads all products from .txt and sorted them by categories.
     * If user wants, choose products by filter
     * Print all products and order
     * @param args never used
     */
    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(System.in);
            String customerName;
            String customerAddress;
            int customerPhone;
            System.out.println("Please write your name:");
            customerName = scanner.nextLine();
            System.out.println("Please write your address:");
            customerAddress = scanner.nextLine();
            System.out.println("Please write your number phone:");
            customerPhone = scanner.nextInt();
            Customer customer = new Customer(customerName, customerAddress, customerPhone);
            ArrayList<Product> products = getProducts();
            ArrayList<Category> categories = getCategories(products);
            System.out.println("There is all products sorted by categories: \n");
            for (Category category : categories) {
                System.out.println(category);
            }
            System.out.println("Would you like to add a filter?(1 - Yes, 0 - No)");
            if(scanner.nextInt() == 1){
                printFilter(scanner, categories);
            }
            System.out.println("Your order: \n" + doOrder(customer, scanner, products));
        }catch (IOException io){
            io.printStackTrace();
        }

    }

    /**
     * This method asked user what he want to buy
     * @param customer who do ordering
     * @param scanner for receiving info from console
     * @param products for giving info about products
     * @return order
     */
    private static Order doOrder(Customer customer, Scanner scanner, ArrayList<Product> products) {
        String productName;
        Order order = new Order(customer, new ArrayList<Product>());
        orderLabel: while(true){
            System.out.println("Please, write product name what you want buy:");
            productName = scanner.nextLine();
            for(Product product : products){
                if(product.getName().equals(productName)){
                    order.addProduct(product);
                    System.out.println(order);
                    System.out.println("Would you like add more product?(1 - Yes, 0 - No)");
                    if(scanner.nextInt() == 0) {
                        break orderLabel;
                    }
                    break;
                }
            }
        }
        return order;
    }

    /**
     * This method receives info from user and return object class Filter
     * @param scanner for receiving info from console
     * @param categories for choosing products
     */
    private static void printFilter(Scanner scanner, ArrayList<Category> categories) {
        Filter filter = null;
        while (true) {
            int minimalPrice;
            int maximalPrice;
            String categoryName = "";
            System.out.println("Write minimal price: ");
            minimalPrice = scanner.nextInt();
            System.out.println("Write maximal price: ");
            maximalPrice = scanner.nextInt();

            categoryLabel:
            while (true) {
                System.out.println("Choose category:\n1 - Aggregate\n2 - Plumbing\n" +
                        "3 - Wood_products\n4 - Paint_Product");
                int categoryNameNum = scanner.nextInt();
                switch (categoryNameNum) {
                    case 1:
                        categoryName = "Aggregate";
                        break categoryLabel;
                    case 2:
                        categoryName = "Plumbing";
                        break categoryLabel;
                    case 3:
                        categoryName = "Wood_products";
                        break categoryLabel;
                    case 4:
                        categoryName = "Paint_Product";
                        break categoryLabel;
                    default:
                        break;
                }
            }
            for (Category category : categories) {
                if (category.getName().equals(categoryName)) {
                    filter = new Filter(category, minimalPrice, maximalPrice);
                }
            }
            if (filter == null || filter.getProducts().isEmpty()) {
                System.out.println("We haven't got any product from your filter. \nWould you like to add a filter?(1 - Yes, 0 - No)");
                if(scanner.nextInt() == 0){
                    return;
                }
            } else {
                System.out.println("Yours chosen products: " + filter);
                return;
            }
        }
    }

    /**
     * This method moves the products and adds them to a certain category
     * @param products list of the products
     * @return list of the categories
     */
    private static ArrayList<Category> getCategories(ArrayList<Product> products) {
        ArrayList<Category> categories = new ArrayList<Category>();
        for (Product product : products) {
            String categoryName = product.getType();
            boolean addedToCategory = false;
            for (Category category : categories) {
                if (category.getName().equals(categoryName)) {
                    category.addProduct(product);
                    addedToCategory = true;
                }
            }
            if (!addedToCategory) {
                categories.add(new Category(categoryName, product));
            }
        }
        return categories;
    }

    /**
     * This method reads products from .txt file, adds them to list and return this list
     * @return list of the products
     * @throws IOException when entered incorrect data
     */
    private static ArrayList<Product> getProducts() throws IOException {
        File file = new File("./src/main/resources/Products.txt");
        BufferedReader br = new BufferedReader(new FileReader(file));
        ArrayList<Product> products = new ArrayList<Product>();
        int productCount = Integer.parseInt(br.readLine());
        for (int i = 0; i < productCount; i++) {
            products.add(new Product(br.readLine(), br.readLine()
                    , br.readLine(),
                    Integer.parseInt(br.readLine()), Double.parseDouble(br.readLine())));
        }
        br.close();
        return products;
    }
}
