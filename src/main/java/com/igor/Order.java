/*
    Main.java

    24.01.2019

    version 1.0
 */
package com.igor;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Class for description order
 *
 * @author igorsas
 * @version 1.0
 * @since 24.01.2019
 */
public class Order {
    /**
     * variable for customer
     */
    private Customer customer;
    /**
     * list of products, which are present in the order
     */
    private ArrayList<Product> products;
    /**
     * general price of the order
     */
    private double price;

    /**
     * Constructor with parameters.
     * For creating object of class Order
     * @param customer who's doing order
     * @param products products, which are present in the order
     */
    public Order(Customer customer, @org.jetbrains.annotations.NotNull ArrayList<Product> products) {
        this.customer = customer;
        this.products = products;
        for(Product product : products){
            price += product.getPrice();
        }
    }

    /**
     * method for getting customer who do ordering
     * @return customer who's doing order
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * method for getting general price of the order
     * @return general price of the order
     */
    public double getPrice() {
        return price;
    }

    /**
     * method for adding new product to the order
     * @param product new product to the order
     */
    public void addProduct(Product product){
        products.add(product);
        for(Product p : products){
            price += p.getPrice();
        }
    }

    /**
     * Overrided method for getting info about object in String
     * @return information about object in String
     */
    @Override
    public String toString(){
        StringBuilder string = new StringBuilder("Order of customer: " + customer + "\nProducts: \n");
        for(Product product : products){
            string.append(product.toString());
        }
        string.append("\nGeneral sum of your order: ").append(price);
        return string.toString();
    }

    /**
     * Overrided method for equals two objects class Order
     * @param o object for equals with object which called this method
     * @return true if objects is equals and false if not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return Double.compare(order.getPrice(), getPrice()) == 0 &&
                Objects.equals(getCustomer(), order.getCustomer()) &&
                Objects.equals(products, order.products);
    }

    /**
     * Overrided method for identification uniq object
     * Uses where we sorted objects in Collection
     * @return uniq hash code for object
     */
    @Override
    public int hashCode() {
        return Objects.hash(getCustomer(), products, getPrice());
    }
}
