/*
    Customer.java

    24.01.2019

    version 1.0
 */
package com.igor;

import java.util.Objects;

/**
 * Class for description customer
 *
 * @author igorsas
 * @version 1.0
 * @since 24.01.2019
 */
public class Customer {
    /**
     * variable for customer name
     */
    private String name;
    /**
     * variable for customer address, where send ordering
     */
    private String address;
    /**
     * variable for customer number phone
     */
    private int phone;

    /**
     * Constructor with parameters.
     * For creating object of class Customer
     * @param name customer name
     * @param address customer current address
     * @param phone customer current number phone
     */
    Customer(String name, String address, int phone){
        this.name = name;
        this.address = address;
        this.phone = phone;
    }

    /**
     * Overrided method for getting info about object in String
     * @return information about object in String
     */
    @Override
    public String toString(){
        return "Customer name: " + name + "\nCustomer address: " +
                address + "\nCustomer phone number: " + phone;
    }
    /**
     * Overrided method for equals two objects class Customer
     * @param o object for equals with object which called this method
     * @return true if objects is equals and false if not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Customer)) return false;
        Customer customer = (Customer) o;
        return getPhone() == customer.getPhone() &&
                Objects.equals(getName(), customer.getName()) &&
                Objects.equals(getAddress(), customer.getAddress());
    }

    /**
     * Overrided method for identification uniq object
     * Uses where we sorted objects in Collection
     * @return uniq hash code for object
     */
    @Override
    public int hashCode() {
        return Objects.hash(getName(), getAddress(), getPhone());
    }

    /**
     * method for getting customer name
     * @return customer name
     */
    public String getName() {
        return name;
    }

    /**
     * method for getting customer address
     * @return customer address
     */
    public String getAddress() {
        return address;
    }

    /**
     * method for setting customer address
     * @param address new customer address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * method for getting customer number phone
     * @return number phone
     */
    public int getPhone() {
        return phone;
    }

    /**
     * method for setting customer number phone
     * @param phone new customer number phone
     */
    public void setPhone(int phone) {
        this.phone = phone;
    }
}
