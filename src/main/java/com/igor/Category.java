/*
    Main.java

    24.01.2019

    version 1.0
 */
package com.igor;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Class for description category
 *
 * @author igorsas
 * @version 1.0
 * @since 24.01.2019
 */
public class Category {
    /**
     * variable for category name
     */
    private String name;
    /**
     * list of products, which belong to the category
     */
    private ArrayList<Product> products;

    /**
     * Constructor with parameters.
     * For creating object of class Category.
     * Create category with only one product
     * @param name category name
     * @param product product which belongs to the category
     */
    public Category(String name, Product product) {
        this.name = name;
        this.products = new ArrayList<>();
        this.products.add(product);
    }

    /**
     * Constructor with parameters.
     * For creating object of class Category.
     * Create category with list od products
     * @param name category name
     * @param products products which belong to the category
     */
    public Category(String name, ArrayList<Product> products) {
        this.name = name;
        this.products = products;
    }

    /**
     * method for getting category name
     * @return category name
     */
    public String getName() {
        return name;
    }

    /**
     * method for setting category name
     * @param name new category name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * method for getting list of products, which belong to the category
     * @return list pf the products which belong to the category
     */
    public ArrayList<Product> getProducts() {
        return products;
    }

    /**
     * method for adding new product to the category
     * @param product new product which belongs to the category
     */
    public void addProduct(Product product){
        products.add(product);
    }

    /**
     * Overrided method for getting info about object in String
     * @return information about object in String
     */
    @Override
    public String toString(){
        StringBuilder string = new StringBuilder("Category name: " + name + "\nProducts: \n");
        for (int i = 0; i < products.size(); i++) {
            string.append(products.get(i));
        }
        string.append("\n");
        return string.toString();
    }

    /**
     * Overrided method for equals two objects class Category
     * @param o object for equals with object which called this method
     * @return true if objects is equals and false if not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Category)) return false;
        Category category = (Category) o;
        return Objects.equals(getName(), category.getName()) &&
                Objects.equals(getProducts(), category.getProducts());
    }

    /**
     * Overrided method for identification uniq object
     * Uses where we sorted objects in Collection
     * @return uniq hash code for object
     */
    @Override
    public int hashCode() {
        return Objects.hash(getName(), getProducts());
    }
}
