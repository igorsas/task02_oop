/*
    Main.java

    24.01.2019

    version 1.0
 */
package com.igor;

import java.util.Objects;

/**
 * Class for description customer
 *
 * @author igorsas
 * @version 1.0
 * @since 24.01.2019
 */
public class Product {
    /**
     * variable for product name
     */
    private String name;
    /**
     * variable for product manufacturer
     */
    private String manufacturer;
    /**
     * variable for product type
     */
    private String type;
    /**
     * variable for count of products
     */
    private int count;
    /**
     * variable for product price
     */
    private double price;

    /**
     * Constructor with parameters.
     * For creating object of class Product
     * @param name product name
     * @param manufacturer product manufacturer
     * @param type product type
     * @param count count of products
     * @param price product price
     */
    public Product(String name, String manufacturer, String type, int count, double price) {
        this.name = name;
        this.manufacturer = manufacturer;
        this.type = type;
        this.count = count;
        this.price = price*count;
    }

    /**
     * method for getting product name
     * @return product namet
     */
    public String getName() {
        return name;
    }

    /**
     * method for setting product name
     * @param name new product name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * method for getting product type
     * @return product type
     */
    public String getType() {
        return type;
    }

    /**
     * method for getting product price
     * @return product price
     */
    public double getPrice() {
        return price;
    }

    /**
     * method for setting product price
     * @param price new product price
     */
    public void setPrice(double price) {
        this.price = price*this.count;
    }

    /**
     * method for getting count of products
     * @return count of products
     */
    public int getCount() {
        return count;
    }

    /**
     * method for setting count of products
     * @param count new count of products
     */
    public void setCount(int count) {
        this.count = count;
        this.price *= count;
    }

    /**
     * Overrided method for getting info about object in String
     * @return info about object in String
     */
    @Override
    public String toString(){
        return "Product name: "
                + name + "\nProduct manufacturer: "
                + manufacturer + "\nProduct count: "
                + count + "\nProduct price: "
                + price + "\n";
    }

    /**
     * Overrided method for equals two objects class Product
     * @param o object for equals with object which called this method
     * @return true if objects is equals and false if not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return getCount() == product.getCount() &&
                Double.compare(product.getPrice(), getPrice()) == 0 &&
                Objects.equals(getName(), product.getName()) &&
                Objects.equals(manufacturer, product.manufacturer) &&
                Objects.equals(getType(), product.getType());
    }

    /**
     * Overrided method for identification uniq object
     * Uses where we sorted objects in Collection
     * @return uniq hash code for object
     */
    @Override
    public int hashCode() {
        return Objects.hash(getName(), manufacturer, getType(), getCount(), getPrice());
    }
}
